﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ResponsiveUIExample
{
    class Worker
    {
        Random rand = new Random();

        public int GenerateNumber()
        {
            Thread.Sleep(1500); //Do it very slowly...
            return rand.Next();
        }
    }
}
